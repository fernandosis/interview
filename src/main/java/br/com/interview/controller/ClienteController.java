package br.com.interview.controller;

import br.com.interview.dto.ClienteDto;
import br.com.interview.model.Cliente;
import br.com.interview.service.ClienteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import java.util.Map;

import static java.util.Objects.isNull;

@Api("Recurso com informacoes de clientes")
@Validated
@RestController
@RequestMapping("/v1/clientes")
public class ClienteController {

    @Autowired private ClienteService clienteService;

    @GetMapping("{id}")
    @ApiOperation(value = "Procura o cliente pelo id informado")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "Cliente econtrado com sucesso", response = ClienteDto.class),
            @ApiResponse(code = 204,message = "Cliente informado nao existe", response = ClienteDto.class),
            @ApiResponse(code = 500,message = "Erro no servidor ao tentar efetuar operacao")
    })
    public ResponseEntity<ClienteDto> procurarPorId(@PathVariable @NotNull Long id){
        return clienteService.procurarPorId(id)
                .map(c -> ResponseEntity.ok(new ClienteDto(c)))
                .orElse(ResponseEntity.noContent().build());
    }

    @PostMapping
    @ApiOperation(value = "Cadastra um cliente novo")
    @ApiResponses(value = {
            @ApiResponse(code = 204,message = "Cliente cadastrado com sucesso", response = ClienteDto.class),
            @ApiResponse(code = 422,message = "Problema ao processar os dados da cliente"),
            @ApiResponse(code = 500,message = "Erro no servidor ao tentar efetuar operacao")
    })
    public ResponseEntity<ClienteDto> cadastrar(@RequestBody @Valid ClienteDto cliente){
        final Cliente clienteCadastrado = clienteService.cadastrarOuAtualizar(new Cliente(cliente));

        return  isNull(clienteCadastrado) ?
                ResponseEntity.unprocessableEntity().build() :
                ResponseEntity.status(HttpStatus.CREATED).body(new ClienteDto(clienteCadastrado));
    }

    @PutMapping
    @ApiOperation(value = "Atualiza todos os dados da cliente pelo")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "Cliente atualizado com sucesso", response = ClienteDto.class),
            @ApiResponse(code = 422,message = "Problema  atualizar o cliente com esses dados"),
            @ApiResponse(code = 500,message = "Erro no servidor ao tentar efetuar operacao")
    })
    public ResponseEntity<ClienteDto> atualizar(@RequestBody ClienteDto cliente){
        final Cliente clienteAtualizada = clienteService.cadastrarOuAtualizar(new Cliente(cliente));

        return  isNull(clienteAtualizada) ?
                ResponseEntity.unprocessableEntity().build() :
                ResponseEntity.ok(new ClienteDto(clienteAtualizada));
    }

    @PatchMapping("{id}")
    @ApiOperation(value = "Atualiza todos os dados da cliente pelo")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "Cliente atualizado com sucesso", response = ClienteDto.class),
            @ApiResponse(code = 204,message = "Cliente nao encontrado pra atualizar", response = ClienteDto.class),
            @ApiResponse(code = 500,message = "Erro no servidor ao tentar efetuar operacao")
    })
    public ResponseEntity<ClienteDto> atualizaItens(@PathVariable Long id , @RequestBody Map<String, Object> parametros){
        final Cliente clienteAtualizada = clienteService.atualizaItesEspecificos(id,parametros);

        return  isNull(clienteAtualizada) ?
                ResponseEntity.noContent().build() :
                ResponseEntity.ok(new ClienteDto(clienteAtualizada));
    }

    @DeleteMapping("{id}")
    @ApiOperation(value = "Remove a cliente pelo id informado")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "Cliente removido com sucesso"),
            @ApiResponse(code = 500,message = "Erro no servidor ao tentar efetuar operacao")
    })
    public void removerPorId(@PathVariable @NotNull Long id){
        clienteService.removerPorId(id);
    }

    @GetMapping
    @ApiOperation(value = "Procura clientes pelos filtros informados")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "Procura realizada com sucesso"),
            @ApiResponse(code = 500,message = "Erro no servidor ao tentar efetuar pesquisa")
    })
    public Page<ClienteDto> procuraPorFiltros(
            @RequestParam(value = "pagina", required = false, defaultValue = "0" ) int pagina,
            @RequestParam(value = "qtd_itens", required = false, defaultValue = "100") int qtdItens,
            @RequestParam(required = false, defaultValue = " ") String nome,
            @RequestParam(required = false, defaultValue = " ") String cpf
    ){
        return clienteService.procuraPorFiltros(nome,cpf,pagina,qtdItens);
    }
}
