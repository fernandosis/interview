package br.com.interview.service;

import br.com.interview.dto.ClienteDto;
import br.com.interview.model.Cliente;
import br.com.interview.repository.ClienteRepository;
import br.com.interview.utils.InterviewUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static br.com.interview.utils.InterviewUtils.*;


@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Optional<Cliente> procurarPorId(Long id){
        return clienteRepository.findById(id);
    }

    public Cliente cadastrarOuAtualizar(Cliente cliente){
        return clienteRepository.save(cliente);
    }

    public Cliente atualizaItesEspecificos(Long id, Map<String, Object> parametros){
        Optional<Cliente> cliente = clienteRepository.findById(id);
        if(cliente.isPresent()){
            parametros.forEach( (nomeParam, parametro) -> {
                switch (nomeParam){
                    case "data_nascimento":
                        cliente.get().setDataNascimento(toDate((String)parametro));
                        break;
                    case "nome":
                        cliente.get().setNome((String) parametro);
                        break;
                    case "cpf":
                        cliente.get().setCpf((String)parametro);
                }
            });
            return clienteRepository.save(cliente.get());
        }
        return null;
    }

    public void removerPorId(Long id){
        clienteRepository.deleteById(id);
    }

    public Page<ClienteDto> procuraPorFiltros(String nome, String cpf, int pagina, int qtdItens){
        Page<Cliente> paginas = clienteRepository.findByNomeContainsOrCpfContains(nome, cpf, PageRequest.of(pagina, qtdItens));
        List<ClienteDto> clientes = paginas.stream().map(c -> new ClienteDto(c)).collect(Collectors.toList());

        return new PageImpl<>(clientes, paginas.getPageable(), paginas.getTotalElements());
    }

}
