package br.com.interview.dto;

import br.com.interview.model.Cliente;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Date;

import static br.com.interview.utils.InterviewUtils.toLocalDate;
import static java.time.temporal.ChronoUnit.YEARS;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClienteDto {

    private Long id;

    @Size(max = 55)
    @NotBlank
    private String nome;

    @Size(max = 11)
    @NotBlank
    private String cpf;

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date data_nascimento;

    public ClienteDto(Cliente cliente){
        this.id = cliente.getId();
        this.nome = cliente.getNome();
        this.data_nascimento = cliente.getDataNascimento();
        this.cpf = cliente.getCpf();
    }

    public Long getIdade(){
        return YEARS.between(toLocalDate(data_nascimento), LocalDate.now());
    }

}
