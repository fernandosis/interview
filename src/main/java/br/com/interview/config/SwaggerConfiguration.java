package br.com.interview.config;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import springfox.documentation.service.Contact;

import static org.apache.logging.log4j.util.Strings.isNotBlank;

@EnableSwagger2
@Configuration
public class SwaggerConfiguration {

    @Autowired
    private Environment env;

    @Bean
    public Docket api(){

        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(metaData());
    }

    public ApiInfo metaData(){

        ApiInfoBuilder apiInfoBuilder = new ApiInfoBuilder();

        String contactName = env.getProperty("interview.swagger.api.contact.name");
        String contactUrl = env.getProperty("interview.swagger.api.contact.url");
        String contactEmail = env.getProperty("interview.swagger.api.contact.email");

        if (isNotBlank(contactName) || isNotBlank(contactUrl) || isNotBlank(contactEmail)){
            apiInfoBuilder.contact(new Contact(contactName,contactUrl, contactEmail));
        }

        String license = env.getProperty("interview.swagger.api.license");

        if (isNotBlank(license))  apiInfoBuilder.license(license);

        String title = env.getProperty("interview.swagger.api.title");
        String description = env.getProperty("interview.swagger.api.description");
        String version = env.getProperty("interview.swagger.api.version");

        if (isNotBlank(title)) apiInfoBuilder.title(title);
        if (isNotBlank(description)) apiInfoBuilder.description(description);
        if (isNotBlank(version)) apiInfoBuilder.version(version);

        return apiInfoBuilder.build();
    }


}