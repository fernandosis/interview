package br.com.interview.repository;

import br.com.interview.model.Cliente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long> {
//    @Query(value = "select c from cliente c where c.nome like %:nome% or c.cpf = :cpf ")
    Page<Cliente> findByNomeContainsOrCpfContains(String nome,String cpf, Pageable page);
}
