package br.com.interview.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class InterviewUtils {
    public static LocalDate toLocalDate(Date date){
        return LocalDate.from(Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()));
    }

    public static Date toDate(String date) {
        SimpleDateFormat dt = new SimpleDateFormat("yyy-MM-dd");
        try {
            return dt.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
