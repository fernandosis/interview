package br.com.interview.enumerated;

public enum Sexo{
    MASCULINO, FEMININO, OUTROS
}