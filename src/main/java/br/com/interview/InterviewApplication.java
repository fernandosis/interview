package br.com.interview;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EntityScan(basePackages = "br.com.interview.model")
@SpringBootApplication(scanBasePackages = {
		"br.com.interview.controller",
		"br.com.interview.service",
		"br.com.interview.repository",
		"br.com.interview.config"
})
public class InterviewApplication {

	public static void main(String[] args) {
		SpringApplication.run(InterviewApplication.class, args);
	}
}
