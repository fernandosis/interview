FROM java:8
COPY /target/interview-builder-0.0.1-SNAPSHOT.jar  interview-builder-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","interview-builder-0.0.1-SNAPSHOT.jar"]